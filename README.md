# Identification of early symptoms on online forums : endometriosis use case

Identification of early symptoms of endometriosis through analysis of online forums.

## Getting started

To carry out this analysis it is necessary to have a corpus of posts on a pathology and a dictionary of symptoms or medical concepts necessary to the study.

The code was applied on a corpus of posts about endometriosis, collected on 10 forums. <br>
The dictionary of medical concepts applied was the MedDRA symptoms (medical concepts felt not to require a diagnosis were qualified as symptoms).  

Each script is presented in a python notebook (Jupyter Notebook) with explanations at each step. Special parameters are indicated at the beginning of each script and can be changed and adjusted. <br>

The **workflow** followed is :
1. *Data source and data extraction:* not described in this directory because it depends on the subject and the desired data
2. *Preprocessing:* data cleaning and formatting step
3. *Symptom detection:* retrieve the symptoms or concepts evoked in the message corpus and their distribution. This step can be followed by an annotation step to select the concepts to be kept in the study and the accepted distributions.
4. *Symptoms contextualization:* bring more precision on the list of detected symptoms (or concepts) by associating each symptom with the most frequently associated words (use of a co-occurrence matrix). 
5. *Early symptoms identification:* keep symptoms (or concepts) frequently close to temporal markers (use of a co-occurrence matrix). An annotation step of the contextualisation words obtained for each concept is necessary to filter out the noise (words that do not make sense).
<br> 
![workflow](/img/workflow_endometriosis_ex.png)

## Preprocessing

The preprocessing steps are as follows:
- Suppression
- Homogeneization
- Stemming

**Post preprocessing**: remove special characters, URLs, text in lower case, without accents and punctuation. The stemming takes each word at its grammatical root (remove conjugations, plural forms)  *postPreprocess.ipynb* <br>

**Medical concepts (or dictionary) preprocessing**: remove short words, text in lower case, without accents and stemmed *dictionaryPreprocess.ipynb*


## Concepts detection

**Aim :** Detect the concepts evoked in the corpus of posts *identifyMedicalConcepts_onPosts.ipynb*. 

**Materials :** Necessary to have a corpus of cleaned posts (obtained with the preprocessing step) and a dictionary of concepts cleaned in the same way (obtained with the preprocessing step). The dictionary of concepts detected must correspond to the subject of the analysis. For the study on endometriosis we used the MedDRA medical concept dictionary. 

**Output :** A list of concepts detected in the posts and their distributions. 

**Application :** After detection of all the MedDRA concepts we proceeded to an annotation to select only the symptoms not requiring any medical procedure and being evoked more than once in the whole corpus of posts.


## Concepts contextualization

**Aim :** Use of a co-occurrence matrix on the corpus of posts to obtain the words frequently close to each concept  *contextualization_ofConceptsLists.ipynb*.  <br>

Here is an example of a stemmed message. The identified concept is "bouffée de chaleur" (hot flash), the words kept in the co-occurrence matrix correspond to the words containing in the window: <br>

![exemple_context](/img/contextualization_concept.png)


**Materials :** Necessary to have a corpus of cleaned posts (obtained with the preprocessing step) and a list of concepts detected on the corpus (obtained with the Concepts detection step). 
<br> It is necessary to enter a **window size** for the co-occurrence matrix. This window will allow the matrix to keep a limited number around the concept detected in a post.

**Output :** A matrix of words representative of the corpus of posts and the list of concepts cleaned in the format of the matrix (without spaces). This matrix is changed into a list of unique concepts with contextualising words. 

## Early concepts identification

**Aim :** Use of a co-occurrence matrix on the corpus of posts to obtain the concepts frequently close to a early temporal marks  *contextualization_ofEarlyConcepts.ipynb*.  <br>

**Materials :** Necessary to have a corpus of cleaned posts filtered on messages containing at least one temporal precocity marker and a list of concepts detected on the corpus (obtained with the Concepts detection step). It is also necessary to have a list of temporal precocity markers obtained from the dictionary of unique words contained in the corpus.
<br> It is necessary to enter a **window size** for the co-occurrence matrix. This window will allow the matrix to keep a limited number around the early temporal marks detected in a post.

**Output :** A matrix of words representative of the dictionary of early temproal marks and the list of concepts cleaned in the format of the matrix (without spaces).

This list will allow the contextualisation matrix of concepts to be filtered to keep only the concepts of precocity. The final list must be annotated to filter out noise (i.e. words associated with a concept that do not provide any information). 


**ANNOTATION STEP**


## Usage
Each step corresponds to a folder available on this git containing python notebooks. Each notebook details the functions and the code used. These scripts can be adapted to other clinical subjects (other pathologies, other concept detection for example risk factors, other periodicity identification).

## Project
This project was realized as part of a thesis on the symptomatology of endometriosis (Master health Data Science at ILIS University of Lille, France).

## Authors 
Fruchart Mathilde (mathilde.frchrt@gmail.com)
El Idrissi Fatima (fatima.elidrissi.etu@univ-lille.fr)

